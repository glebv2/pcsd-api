from setuptools import setup
setup(
    name='pcsd-api',
    version='1.1',
    description='Pcsd api',
    url='https://git.glebmail.xyz/PythonPrograms/pcsd',
    author='gleb',
    packages=['pcsdapi'],
    author_email='gleb@glebmail.xyz',
    license='GNU GPL 3',
)
